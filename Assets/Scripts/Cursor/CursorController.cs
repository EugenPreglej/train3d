using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    private Nodes nodes;


    void Awake()
    {
        nodes = FindObjectOfType<Nodes>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 5000f))
            {
                if (hit.collider != null)
                {
                    if (hit.transform.GetComponentInParent<Nodes>())
                    {
                        nodes.onClickAction();
                    }
                }
            }
        }
    }
 }

