using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines.Examples;

public class Nodes : MonoBehaviour
{
    public GameObject nodes;
    private JunctionSwitch junctionSwitch;
    private Panel panel;

    void Awake() 
    {
        panel = FindObjectOfType<Panel>();
    }


    void Start()
    {
        foreach (Transform child in transform)
        {
            junctionSwitch = child.GetComponent<JunctionSwitch>();
            foreach (JunctionSwitch.Bridge bridge in junctionSwitch.bridges)
            {
                bridge.active = false;
            }
        }
        
    }

    public void onClickAction()
    {
        panel.changeTitle(this.name);
        panel.Open();
    }

    public void changeBridgeState(JunctionSwitch.Bridge bridge)
    {
        bool state = bridge.active;
        bridge.active = !state;
    }
}
