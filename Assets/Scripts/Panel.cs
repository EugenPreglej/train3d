    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using TMPro;

    public class Panel : MonoBehaviour
    {
        public RectTransform panel;
        public RectTransform background;
        public TMP_Text title;

        private GameObject intersection;
        private Vector3 startingPanelSize;
        private Vector3 startingBackgroundSize;
        private Vector3 minimized;

        void Start()
        {
            Time.timeScale = 0;
            minimized = new Vector3(0, 0, 0);
            startingPanelSize = panel.localScale;
            startingBackgroundSize = background.localScale;
            Close();
        }

        public void changeTitle(string text)
        {
            title.SetText(text);
        }

        public void Run()
        {
            Time.timeScale = 1;
        }

        public void Open()
        {
            intersection = GameObject.Find(title.text);     //pronalazimo otvoreni intersection
            panel.localScale = startingPanelSize;
            background.localScale = startingBackgroundSize;
        }

        public void Close()
        {
            panel.localScale = minimized;
            background.localScale = minimized;
        }
    }
